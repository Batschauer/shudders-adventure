﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toques : MonoBehaviour {

    public Text text;
	
	// Update is called once per frame
	void Update () {

        foreach (Touch toque in Input.touches) {
            string mensagem = "";
            mensagem += "ID:" + toque.fingerId + "\n";
            mensagem += "Phase:" + toque.phase.ToString() + "\n";
            mensagem += "TapCount:" + toque.tapCount + "\n";
            mensagem += "Pos X: " + toque.position.x + "\n";
            mensagem += "Pos Y: " + toque.position.y + "\n";
            text.text = mensagem;

        }
	}
}
